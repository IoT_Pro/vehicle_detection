
import os
import sys
import cv2
import numpy as np


LABELS = ["background", "aeroplane", "bicycle", "bird", "boat",
          "bottle", "bus", "car", "cat", "chair",
          "cow", "diningtable", "dog", "horse", "motorbike",
          "person", "pottedplant", "sheep", "sofa", "train", "tvmonitor"]
PERSON = [15]
TRAFFIC_OBJs = [6, 7, 19]
VEHICLES = [7]


_cur_dir = os.path.dirname(os.path.realpath(__file__))


class VehicleDetect:
    def __init__(self, targets=VEHICLES, model_path=_cur_dir + "/models/ssd_mobile", ssd_sz=None):
        if targets is None:
            targets = []
        prototxt = model_path + "/MobileNetSSD_deploy.prototxt"
        model = model_path + "/MobileNetSSD_deploy.caffemodel"
        if not os.path.exists(prototxt) or not os.path.exists(model):
            sys.stderr.write("no exist SSDmobile models\n")
            return
        else:
            print("loading ssd-mobile models...")
            self.net = cv2.dnn.readNetFromCaffe(prototxt, model)

        if len(targets) == 0:
            self.targets = range(1000)
        else:
            self.targets = targets

        self.confidence = 0.5

        if ssd_sz is None:
            self.ssd_im_sz = (300, 200)
        else:
            self.ssd_im_sz = ssd_sz

    def detect(self, img):
        blob = cv2.dnn.blobFromImage(cv2.resize(img, self.ssd_im_sz), 0.007843, self.ssd_im_sz, 127.5)

        self.net.setInput(blob)
        detections = self.net.forward()

        vehicles = []
        for i in np.arange(0, detections.shape[2]):
            confidence = detections[0, 0, i, 2]
            idx = int(detections[0, 0, i, 1])

            if confidence > self.confidence and idx in self.targets:
                rect = detections[0, 0, i, 3:7]
                vehicles.append({'label': LABELS[idx],
                                 'confidence': confidence,
                                 'rect': rect})

        return vehicles

    def draw_objects(self, dets, frame):
        h, w = frame.shape[:2]
        for obj in dets:
            [x, y, x2, y2] = (obj['rect'] * np.array([w, h, w, h])).astype(int)
            cv2.rectangle(frame, (x, y), (x2, y2), (0, 0, 255), 2)
        return frame

if __name__ == '__main__':
    vehicle = VehicleDetect(TRAFFIC_OBJs)
    # drawer = Draw()

    # folder = "./data/videos/OneDrive-2018-07-22/1mp 2.8mm aussenparkplatz"
    # paths = [os.path.join(folder, fn) for fn in os.listdir(folder)]
    # path = paths[5]

    cap = cv2.VideoCapture("car_2.mp4")
    width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
    height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
    fps = cap.get(cv2.CAP_PROP_FPS)

    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    saver = cv2.VideoWriter("output.mp4", fourcc, fps, (int(width), int(height)))

    while True:
        ret, frame = cap.read()
        if not ret:
            break
        objects = vehicle.detect(img=frame)
        result = vehicle.draw_objects(dets=objects, frame=frame)

        saver.write(result)

        cv2.imshow("result", result)
        key = cv2.waitKey(1)
        if key == ord('q'):
            break

    saver.release()
    cap.release()
    cv2.destroyAllWindows()
